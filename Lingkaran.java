import java.util.Scanner;

public class Lingkaran {
    static void LuasLingkaran() {
        Scanner userInput = new Scanner(System.in);

        //Deklarasi Variabel
        int r;
        double luas,phi=3.14;

        System.out.println("\t Menghitung Luas Lingkaran ");
        System.out.println("======================================");

        //input nilai jari-jari
        System.out.print(" Masukkan jari-jari : ");
        r = userInput.nextInt();

        //Menghitung luas lingkaran
        luas = phi * r * r;

        //Tampilkan hasil
        System.out.println("\n Luas Lingkaran adalah : " + luas);
        System.out.println("======================================");
    }
}
