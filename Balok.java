import java.util.Scanner;

public class Balok {
    static void VolumeBalok() {
        Scanner nilai = new Scanner(System.in);

        int volume, p, l, t;

        System.out.println("\t Anda memilih Balok ");
        System.out.println("======================================");
        System.out.print("Masukkan nilai panjang : ");
        p = nilai.nextInt();
        System.out.print("Masukkan nilai lebar : ");
        l = nilai.nextInt();
        System.out.print("Masukkan nilai tinggi : ");
        t = nilai.nextInt();

        volume = p * l * t;
        System.out.println(" Hasilnya adalah : " + volume + " cm kubik");
        System.out.println("======================================");
    }
}
