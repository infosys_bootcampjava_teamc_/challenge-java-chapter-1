import java.util.Scanner;

public class LuasPersegi {
    static void Persegi() {
        Scanner userInput = new Scanner(System.in);

        int sisi,luas;

        System.out.println("\t Menghitung Luas Persegi ");
        System.out.println("======================================");

        System.out.print(" Masukkan sisi Persegi : ");
        sisi = userInput.nextInt();

        luas = sisi * sisi;

        System.out.println("\n Luas Persegi adalah : " + luas);
        System.out.println("======================================");
    }
}
