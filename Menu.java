import java.util.Scanner;

public class Menu {
    static Scanner input = new Scanner(System.in);

    boolean mainLoop = true;

    public static void main(String[] args) {

        Menu object = new Menu(); //membuat dari class Menu
        menu_utama(); //memanggil metode void menu

    }

    public static void menu_utama() { //method void menunya

        System.out.println("======================================");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("======================================");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("======================================");

        System.out.print("Pilihan anda : ");
        String pilihan = input.nextLine(); //menginput pilihan


            switch (pilihan) {
            case "1" : //jika pilihan satu maka akan muncul menu luas
            do {
                System.out.println("======================================");
                System.out.println("Pilih Bidang yang akan di hitung");
                System.out.println("======================================");
                System.out.println("1. Segitiga");
                System.out.println("2. Persegi");
                System.out.println("3. Lingkaran");
                System.out.println("4. Persegi Panjang");
                System.out.println("0. Kembali Ke Menu Sebelumnya");
                System.out.println("======================================");
                System.out.print(" Masukkan No Bidang Yang ingin di pilih : ");
                pilihan = input.nextLine();


                switch (pilihan) {

                    case "1" -> LuasSegitiga.Segitiga(); //manggil kelas LuasSegitiga sama menjalankan coding di kelas LuasSegitiga
                    case "2" -> LuasPersegi.Persegi();
                    case "3" -> Lingkaran.LuasLingkaran();
                    case "4" -> LuasPersegiPanjang.PersegiPanjang();
                    //jika pilih 0 maka akan kembali ke menu awal
                    case "0" -> menu_utama(); //kembali menampilkan si void menu utama

                    default -> { // jika tidak ada di pilihan atas maka akan mengeksekusi ini
                        System.out.println("======================================");
                        System.out.println("eitss kelebihan angkanya! ");
                    }
                }
            }
            while (!"5".equals(pilihan)) ;

                break;
            case "2":
            do {
                System.out.println("======================================");
                System.out.println(" Pilih Bangun Ruang Yang Akan Dihitung ");
                System.out.println("======================================");
                System.out.println(" 1. Kubus ");
                System.out.println(" 2. Balok ");
                System.out.println(" 3. Tabung ");
                System.out.println(" 0. Kembali Ke Halaman Pertama ");
                System.out.println("======================================");
                System.out.print(" Masukkan Pilihan Kamu : ");
                pilihan = input.nextLine();
                switch (pilihan) {
                    case "1":
                        Kubus.VolumeKubus();
                        break;
                    case "2":
                        Balok.VolumeBalok();
                        break;
                    case "3":
                        Tabung.VolumeTabung();
                        break;
                    case "0":
                        menu_utama();

                    default:
                        //System.exit(0);
                        System.out.println("eitss kelebihan angkanya! ");
                }
            }
            while (!"4".equals(pilihan)) ;
                break;

                case "0":
                System.out.println("Menutup Program Aplikasi");
                System.out.println("======================================");
                System.exit(0);
                break;

            default:
//                System.exit(0);
                System.out.println("eitss kelebihan angkanya! ");
        }

    }

}
