import java.util.Scanner;

public class Tabung {
    static void VolumeTabung() {
        Scanner nilai = new Scanner(System.in);

        double volTabung, r, t;
        final double phi = 3.14;

        System.out.println("\t Anda memilih Tabung");
        System.out.println("======================================");
        System.out.print("Masukkan nilai diameter : ");
        r = nilai.nextDouble();
        System.out.print("Masukkan nilai tinggi : ");
        t = nilai.nextDouble();

        volTabung = (phi * (r/2) * (r/2)) * t;
        System.out.println("Hasilnya adalah : " + volTabung + " cm kubik");
        System.out.println("======================================");
    }
}
