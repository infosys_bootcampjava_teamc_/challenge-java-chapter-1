import java.util.Scanner;

public class LuasSegitiga {
    static void Segitiga() {
        Scanner userInput = new Scanner(System.in);

        int alas, tinggi;
        double luas;

        System.out.println("\t Menghitung Luas Segitiga ");
        System.out.println("======================================");

        System.out.print(" Masukkan Alas   : ");
        alas = userInput.nextInt(); //masukkin inputtan nilai alasnya

        System.out.print(" Masukkan Tinggi : ");
        tinggi = userInput.nextInt(); //masukkin inputtan nilai tingginya

        luas = (alas * tinggi) * 0.5; // rumusnya

        System.out.println("\n Luas Segitiga adalah : " + luas);
        System.out.println("======================================");
    }
}
