import java.util.Scanner;

public class Kubus {
    static void VolumeKubus() {
        Scanner nilai = new Scanner(System.in);

        int volume,s;

        System.out.println("\t Anda memilih Kubus ");
        System.out.println("======================================");
        System.out.print("Masukkan nilai sisi : "); //input nilai sisi
        s = nilai.nextInt();

        volume = s * s * s; //menghitung volume Kubus
        System.out.println("Hasilnya adalah : " + volume + " cm kubik"); //Tampilan Hasil
        System.out.println("======================================");
    }
}
