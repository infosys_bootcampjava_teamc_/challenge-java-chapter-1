import java.util.Scanner;

public class LuasPersegiPanjang {
     static void PersegiPanjang() {
        Scanner userInput = new Scanner(System.in);

        int panjang,lebar,luas;

        System.out.println("\t Menghitung Luas Persegi Panjang ");
        System.out.println("======================================");

        System.out.print(" Masukkan Panjang : ");
        panjang = userInput.nextInt();

        System.out.print(" Masukkan Lebar : ");
        lebar = userInput.nextInt();

        luas = panjang * lebar;

        System.out.println("\n Luas Persegi Panjang adalah : " + luas);
        System.out.println("======================================");
    }
}
